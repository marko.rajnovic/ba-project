import React from "react";
import './Header.css'
import favicon from "../../assets/favicon.png"

export const Header = () => {
    return (
        <div className="header-cnt">
            <img className="header-img" src={favicon} alt={""}/>
            <p className="header-title">Hahackathon</p>
        </div>
    );
}