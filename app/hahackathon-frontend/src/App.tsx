import React from 'react';
import './App.css';
import {BrowserRouter} from "react-router-dom";
import {Routes} from "./Routes";
import {Header} from "./components/Header/Header";

function App() {
  return (
      <BrowserRouter>
          <Header/>
          <div className={"main__cnt"}>
              <Routes/>
          </div>
      </BrowserRouter>
  );
}

export default App;
