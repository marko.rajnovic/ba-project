package takelab.projekt.hahackathon.backend.services.impl;

import org.springframework.stereotype.Service;
import takelab.projekt.hahackathon.backend.dtos.responses.HumorEvaluationResponse;
import takelab.projekt.hahackathon.backend.services.HumorEvaluatorService;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HumorEvaluatorServiceImpl implements HumorEvaluatorService {

    private static final String IS_HUMOR_TRUE = "1";

    @Override
    public HumorEvaluationResponse calculateHumor(String text) throws IOException {

        ProcessBuilder processBuilder = new ProcessBuilder("python", resolvePythonScriptPath("pipeline.py"));
        processBuilder.redirectErrorStream(true);
        Process process = processBuilder.start();

        OutputStream stdin = process.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stdin));

        writer.write(text);
        writer.flush();
        writer.close();

        List<String> results = readProcessOutput(process.getInputStream());
        String pov = results.get(results.size() - 1);
        String[] splitted = pov.split(";");
        String povValue = splitted[0];
        Double humorRating;
        Double offenseRating;

        try {
            humorRating = Double.parseDouble(splitted[1].substring(1, splitted[1].length() - 1));
        } catch (Exception ex) {
            humorRating = 0.0;
        }

        try{
            offenseRating = Double.parseDouble(splitted[2].substring(1, splitted[2].length() - 1));
        } catch (Exception ex) {
            offenseRating = 0.0;
        }

        HumorEvaluationResponse response = new HumorEvaluationResponse();

        return mapResponse(response, povValue, text, humorRating, offenseRating);
    }

    private HumorEvaluationResponse mapResponse(HumorEvaluationResponse response, String povValue, String text, Double humorRating, Double offenseRating) {
        response.setText(text);
        response.setHumor(povValue.equals(IS_HUMOR_TRUE));
        response.setHumorRating(humorRating);
        response.setOffenseRating(offenseRating);

        return response;
    }


    private List<String> readProcessOutput(InputStream inputStream) throws IOException {
        try (BufferedReader output = new BufferedReader(new InputStreamReader(inputStream))) {
            return output.lines()
                    .collect(Collectors.toList());
        }
    }

    private String resolvePythonScriptPath(String filename) {
        File file = new File(filename);
        return file.getAbsolutePath();
    }
}
