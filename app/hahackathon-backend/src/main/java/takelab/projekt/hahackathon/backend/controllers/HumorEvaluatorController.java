package takelab.projekt.hahackathon.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import takelab.projekt.hahackathon.backend.services.HumorEvaluatorService;

import java.io.IOException;

@RestController
@RequestMapping("/calculate-humor")
public class HumorEvaluatorController {

    private final HumorEvaluatorService humorEvaluatorService;

    @Autowired
    public HumorEvaluatorController(HumorEvaluatorService humorEvaluatorService) {
        this.humorEvaluatorService = humorEvaluatorService;
    }

    @GetMapping
    public ResponseEntity<?> calculateHumor(@RequestParam String text) {
        try {
            return ResponseEntity.ok(humorEvaluatorService.calculateHumor(text));
        } catch (IOException exception) {
            return ResponseEntity.badRequest().body("Dogodila se pogreška prilikom procjene humora. Pokušajte kasnije.");
        }
    }

}
