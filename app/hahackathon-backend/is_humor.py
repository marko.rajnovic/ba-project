import numpy as np
import spacy
from tensorflow.keras import models, optimizers, preprocessing as kprocessing
from tensorflow import keras
import pickle

nlp = spacy.load("en_core_web_sm")
#tokenizirano
tokenizer = pickle.load(open('tokenizer_glove50d_reduced.pickle', 'rb'))
model = keras.models.load_model('model.h5')

def predict_for_text(text):
    text = text.lower()
    tokenized = nlp.make_doc(text)
    lemma = [token.lemma_ for token in tokenized]
    # print(lemma) 
    
    text_corpus = list(lemma)
    seq_text = np.hstack(tokenizer.texts_to_sequences(text_corpus)).astype(int)
    seq_text = np.reshape(seq_text, (1, -1))
    
    X_text = kprocessing.sequence.pad_sequences(seq_text
                                                 , maxlen=21
                                                 , padding="post"
                                                 , truncating="post")
    # print(X_text)
    
    y_hat = model.predict(X_text)
    y_hat

    y_pred = np.argmax(y_hat)
    
    print(y_pred, ";0;0")
	
text = input()
predict_for_text(text)